﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorTest : MonoBehaviour
{
    [SerializeField]
    int _numPoints = 10;

    [SerializeField]
    float _angle = 0f;

    [SerializeField]
    int _seed = 0;

    [SerializeField]
    float _scale = 3.24f;

    void OnDrawGizmos()
    {
        var vs = PerlinVector();
        for(int i = 0; i < vs.Length; ++i )
        {
            var position = transform.position + Vector3.right * i;
            Gizmos.DrawLine( position, position + vs[ i ] );
        }
    }

    Vector3[] RandomVector()
    {
        Random.InitState( _seed );
        var vs = new Vector3[_numPoints];
        for( int i = 0; i < _numPoints; ++i )
        {
            vs[ i ] = Random.onUnitSphere;
        }
        return vs;
    }

    Vector3[] PerlinVector()
    {
        var vs = new Vector3[_numPoints];
        for( int i = 0; i < _numPoints; ++i )
        {
            vs[ i ].x = Mathf.PerlinNoise( i * _scale, 1f * _scale ) * 2f - 1f;
            vs[ i ].y = Mathf.PerlinNoise( i * _scale, 2f * _scale ) * 2f - 1f;
            vs[ i ].z = Mathf.PerlinNoise( i * _scale, 3f * _scale ) * 2f - 1f;
        }
        return vs;
    }
}
