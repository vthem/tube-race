﻿using UnityEngine;

public class Hermite
{
    Vector3 _p0, _p1, _v0, _v1;

    public Hermite( Vector3 p0, Vector3 v0, Vector3 p1, Vector3 v1 )
    {
        _p0 = p0;
        _p1 = p1;
        _v0 = v0;
        _v1 = v1;
    }

    public Vector3 Point( float t )
    {
        var t2 = t * t;
        var t3 = t * t * t;
        var h0 = 1 - 3f * t2 + 2 * t3;
        var h1 = t - 2 * t2 + t3;
        var h2 = -t2 + t3;
        var h3 = 3 * t2 - 2 * t3;
        return h0 * _p0 + h1 * _v0 + h2 * _v1 + h3 * _p1;
    }

}
