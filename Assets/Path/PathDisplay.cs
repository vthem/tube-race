﻿using UnityEngine;

public class PathDisplay : MonoBehaviour
{
    public enum Method
    {
        Subdivision,
        Additive,
        Circle
    }

    [SerializeField]
    int _numPoints = 10;

    [SerializeField]
    float _minSectionLength = 1f;

    [SerializeField]
    float _maxSectionLength = 5f;

    [SerializeField]
    int _seed = 0;

    [SerializeField]
    int _subDivision = 2;

    [SerializeField]
    [Range(0f, 1f)]
    float _persistence = .5f;

    [SerializeField]
    int _hermiteSubdvision = 10;

    [SerializeField]
    float _hermiteScale = 1f;


    [SerializeField]
    Method _method;

    [SerializeField]
    float _count;

    Bounds b = new Bounds();

    private void OnDrawGizmos()
    {
        if( _method == Method.Additive )
        {
            Draw_Additive();
        }
        else if( _method == Method.Subdivision )
        {
            Draw_Subdivision();
        }
        else if (_method == Method.Circle )
        {

        }
    }

    private void OnValidate()
    {
        _numPoints = Mathf.Max( 1, _numPoints );
        _subDivision = Mathf.Max( 1, _subDivision );
        _subDivision = Mathf.Min( _subDivision, 20 );
        _hermiteSubdvision = Mathf.Min( _hermiteSubdvision, 4000 );
        _hermiteSubdvision = Mathf.Max( 1, _hermiteSubdvision );
    }

    void Draw_Additive()
    {
        Gizmos.color = Color.white;
        var path = Path.Generate(transform.position, _seed, _numPoints, _minSectionLength, _maxSectionLength);
        for( int i = 0; i < path.Length; ++i )
        {
            Gizmos.DrawLine( path[ i ].position, path[ i ].End );
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube( Path.failBounds.center, Path.failBounds.size );
        Gizmos.color = Color.red;
        Gizmos.DrawLine( Path.last.position, Path.last.End );

    }

    void Draw_Subdivision()
    {
        //Gizmos.color = Color.white;
        //var path = Path.Split(transform.position, (Mathf.Pow(2, _subDivision)+1) * _minSectionLength, _seed, _subDivision, _persistence);
        //_count = path.Length;
        //for( int i = 0; i < path.Length - 2; ++i )
        //{
        //    var hermite = new Hermite(path[ i ], (path[ i + 1 ] - path[ i ]).normalized * _hermiteScale, path[ i + 1 ],  (path[ i + 2 ] - path[ i + 1 ]).normalized * _hermiteScale);
        //    var lastPoint = path[ i ];
        //    for( int t = 1; t < _hermiteSubdvision; ++t )
        //    {
        //        var p = hermite.Point(t/(float)_hermiteSubdvision);
        //        Gizmos.DrawLine( lastPoint, p );
        //        lastPoint = p;
        //    }
        //    Gizmos.DrawLine( lastPoint, path[ i + 1 ] );
        //}
        //Gizmos.color = Color.yellow;
        //Gizmos.DrawLine( transform.position, path[ path.Length - 1 ] );
    }
}
