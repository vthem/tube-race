﻿using System.Collections.Generic;
using UnityEngine;

public struct PathPoint
{
    public Vector3 position;
    public Vector3 direction;
    public float length;

    public Vector3 End { get { return position + direction * length; } }
}

public class Path
{
    public static PathPoint last;
    public static Bounds failBounds;

    public static Vector3[] CircularPath( Vector3 start, float radius, int numPoints, int seed )
    {
        Random.InitState( seed );
        var center = Random.onUnitSphere;
        var dir = (center - start).normalized;
        var q = Quaternion.FromToRotation( Vector3.right, dir );
        var f = q * Vector3.forward;
        var u = q * Vector3.up;

        var points = new Vector3[numPoints];
        //for( int i = 0; i < numPoints; ++i )
        //    points[ i ] = radius * ( Quaternion.AngleAxis( 360f * i / ( numPoints - 1 ), direction ) * start );
        return points;
    }

    public static Vector3[] Split( Vector3 from, Vector3 to, int count )
    {
        var result = new Vector3[count];
        for( int i = 0; i < count; ++i )
            result[ i ] = Vector3.Lerp( from, to, i / (float)( count - 1 ) );
        return result;
    }

    public static Vector3[] 

    public static PathPoint[] Generate( Vector3 position, int seed, int numPoints, float minSegmentLength, float maxSegmentLength )
    {
        var path = new PathPoint[numPoints];
        Random.InitState( seed );
        Bounds pathBounds = new Bounds();
        var pathPoint = RandomPathPoint( position, minSegmentLength, maxSegmentLength, pathBounds );
        pathBounds.Encapsulate( pathPoint.End );
        path[ 0 ] = pathPoint;
        for( int i = 1; i < numPoints; ++i )
        {
            pathPoint = RandomPathPoint( path[ i - 1 ].End, minSegmentLength, maxSegmentLength, pathBounds );
            //if( pathBounds.Contains( pathPoint.End ) )
            //{
            //    failBounds = pathBounds;
            //    last = pathPoint;
            //    break;
            //}
            pathBounds.Encapsulate( pathPoint.End );
            path[ i ] = pathPoint;
        }
        return path;
    }

    static PathPoint RandomPathPoint( Vector3 position, float minSegmentLength, float maxSegmentLength, Bounds pathBounds )
    {
        var pp = new PathPoint();
        pp.position = position;
        pp.length = Random.Range( minSegmentLength, maxSegmentLength );
        do
        {
            pp.direction = Random.onUnitSphere;
        }
        while( pathBounds.Contains( pp.End ) );
        //pp.direction.y = RandValue( pathBounds.min.y, pathBounds.max.y, pp.position.y );
        //pp.direction.z = RandValue( pathBounds.min.z, pathBounds.max.z, pp.position.z );
        return pp;
    }

    static float RandValue( float min, float max, float p )
    {
        if( p >= max )
            return Random.Range( 0f, 1f );
        else if( p <= min )
            return Random.Range( -1f, 0f );
        return Random.Range( -1f, 1f );
    }


}
