﻿using UnityEngine;

public class PathSplitterDisplay : MonoBehaviour
{
    [Range(0.001f, 1f)]
    public float limit;
    public float persistence;

    private void OnDrawGizmos()
    {
        Random.InitState( 0 );
        var points = PathSplitter.Split(new Vector3[2] {-transform.right, transform.right }, persistence, limit );
        for( int i = 1; i < points.Length; i++ )
        {
            Gizmos.DrawLine( points[ i ], points[ i - 1 ] );
        }
    }
}
