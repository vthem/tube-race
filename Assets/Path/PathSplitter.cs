﻿using UnityEngine;

public class PathSplitter {

    public static Vector3[] Split( Vector3[] input, float persistence, float limit )
    {
        var lastPersitence = 1f;
        Vector3[] next;
        while( Split( input, lastPersitence * persistence, limit, out next ) )
        {
            input = next;
            lastPersitence = persistence;
        }
        return next;
    }

    public static bool Split( Vector3[] input, float persistence, float limit, out Vector3[] output )
    {
        var numPoints = (input.Length - 1) * 2 +1;
        output = new Vector3[ numPoints ];
        var p = Vector3.zero;
        bool stop = false;
        for( int i = 0; i < input.Length - 1; i++ )
        {
            output[ 2 * i ] = input[ i ];
            output[ 2 * i + 1 ] = Split( input[ i ], input[ i + 1 ], persistence );
            if( ( output[ 2 * i + 1 ] - output[ 2 * i ] ).sqrMagnitude < limit )
                stop = true;
        }
        output[ 2 * ( input.Length - 1 ) ] = input[ input.Length - 1 ];
        return stop;
    }

    static Vector3 Split( Vector3 p1, Vector3 p2, float persistence )
    {
        var dir = (p1-p2).normalized;
        Vector3 any;
        if( dir.z == 0f )
            any = Vector3.forward;
        else
            any = new Vector3( 1, 1, -( dir.x + dir.y ) / dir.z ).normalized;
        return .5f * ( p1 + p2 ) + ( p1 - p2 ).magnitude * persistence * ( Quaternion.AngleAxis( Random.Range( 0, 360f ), dir ) * any );
    }
}
