﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleTest : MonoBehaviour
{
    public int seed;
    public int numPoints;
    public float radius;
    public float circleScale;
    public int loopCount = 10;
    [Range(0.1f, 1f)]
    public float persistence;
    public float limit = 400f;

    private void OnDrawGizmos()
    {
        Random.InitState( seed );
        var start = Vector3.zero;
        var circle = CirclePoints(-transform.up, transform.right, numPoints);
        Gizmos.DrawLine( transform.position, transform.position + transform.right * radius );
        for( int i = 0; i < loopCount; i++ )
        {
            var offset = RandomPoints(numPoints);
//            offset[ 0 ] = start;

            var wPoints = WorldPoints(transform.position, circle, radius, offset, circleScale);
            DrawPath( wPoints, Color.red );
            var sPoints = Path.Split(wPoints, persistence, limit);
            DrawPath( sPoints, Random.ColorHSV() );
            start = offset[ offset.Length - 1 ];
        }
    }

    private void OnValidate()
    {
        numPoints = Mathf.Max( 3, numPoints );
        radius = Mathf.Max( 1f, radius );
        loopCount = Mathf.Max( 0, loopCount );
    }

    private static Vector3[] CirclePoints( Vector3 forward, Vector3 start, int numPoints )
    {
        var points = new Vector3[numPoints];
        for( int i = 0; i < numPoints; ++i )
            points[ i ] = ( Quaternion.AngleAxis( 360f * i / ( numPoints - 1 ), forward ) * start );
        return points;
    }

    private static Vector3[] RandomPoints( int numPoints )
    {
        var points = new Vector3[numPoints];
        for( int i = 1; i < numPoints; i++ )
            points[ i ] = Random.onUnitSphere;
        return points;
    }

    private static Vector3[] WorldPoints( Vector3 center, Vector3[] init, float radius, Vector3[] offset, float scale )
    {
        var result = new Vector3[init.Length];
        for( int i = 0; i < init.Length; i++ )
        {
            result[ i ] = center + init[ i ] * radius + offset[ i ] * scale;
        }
        return result;
    }

    private static void DrawPath( Vector3[] points, Color color )
    {
        Gizmos.color = color;
        for( int i = 1; i < points.Length; i++ )
        {
            Gizmos.DrawLine( points[ i ], points[ i - 1 ] );
        }
    }
}
