﻿using UnityEngine;

public class TubeMeshDisplay : MonoBehaviour
{
    [SerializeField]
    int _numPoint = 10;

    [SerializeField]
    float _length = 4f;

    [SerializeField]
    float _radius = 4f;

    [SerializeField]
    bool _debugDrawMeshVertex = false;

    [SerializeField]
    Vector3 _beginDirection;

    [SerializeField]
    Vector3 _endDirection;

    Mesh _mesh;

    public float Length { get { return _length; } set { _length = value; } }
    public Vector3 BeginDirection { get { return _beginDirection; } set { _beginDirection = value.normalized; } }
    public Vector3 EndDirection { get { return _endDirection; } set { _endDirection = value.normalized; } }

    public static void Create( int numPoint, float length, float radius, Vector3 beginDirection, Vector3 endDirection, Transform parent )
    {
        var obj = new GameObject();
        var tmd = obj.AddComponent<TubeMeshDisplay>();
        tmd._numPoint = numPoint;
        tmd._length = length;
        tmd._radius = radius;
        tmd._beginDirection = beginDirection;
        tmd._endDirection = endDirection;
        tmd._debugDrawMeshVertex = false;
        tmd.transform.SetParent( parent );
    }

    public void UpdateMesh()
    {
        if( null == _mesh )
        {
            _mesh = new Mesh();
            GetComponent<MeshFilter>().sharedMesh = _mesh;
        }
        TubeMesh.Generate( _mesh, _numPoint, _radius, _length, _beginDirection, _endDirection );
    }

    private void OnValidate()
    {
        _numPoint = Mathf.Max( 2, _numPoint );
    }

    void OnDrawGizmos()
    {
        if( _debugDrawMeshVertex && _mesh != null )
        {
            Gizmos.color = Color.yellow;
            var vertices = _mesh.vertices;
            for( int i = 0; i < vertices.Length; ++i )
            {
                Gizmos.DrawSphere( transform.position + vertices[ i ], .1f );
            }
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawLine( transform.position, transform.position + BeginDirection );
    }

    void OnDestroy()
    {
        if( _mesh != null )
            Destroy( _mesh );
    }
}
