﻿
using UnityEngine;

public static class TubeMesh
{
    public static void Generate( Mesh mesh, int numPoint, float radius, float length, Vector3 beginDirection, Vector3 endDirection )
    {
        mesh.Clear();

        var triCount = (numPoint * 2) - 2;
        var vertices = new Vector3[ triCount * 3 ];
        var beginPoints = CirclePoints(beginDirection, numPoint, radius);
        var endPoints = CirclePoints(endDirection, numPoint, radius);
        for( int i = 0, j = 0; i < vertices.Length; i += 6, j = i / 6)
        {
            vertices[ i ] = beginPoints[ j ];
            vertices[ i + 1 ] = endPoints[ j ] + beginDirection * length;
            vertices[ i + 2 ] = beginPoints[ j + 1 ];
            vertices[ i + 3 ] = vertices[ i + 1 ];
            vertices[ i + 4 ] = endPoints[ j  + 1] + beginDirection * length;
            vertices[ i + 5 ] = vertices[ i + 2 ];
        }
        mesh.vertices = vertices; 

        var tri = new int[triCount * 3];
        for( int i = 0; i < triCount * 3; ++i )
        {
            tri[ i ] = i;
        }
        mesh.triangles = tri;

        var uv = new Vector2[vertices.Length];

        //uv[ 0 ] = new Vector2( 0, 0 );
        //uv[ 1 ] = new Vector2( 1, 0 );
        //uv[ 2 ] = new Vector2( 0, 1 );
        //uv[ 3 ] = new Vector2( 1, 1 );

        mesh.uv = uv;
        mesh.RecalculateNormals();
    }

    public static Vector3[] CirclePoints( Vector3 direction, int numPoints, float radius = 1f )
    {
        Vector3 start;
        if( direction.z == 0f )
            start = Vector3.forward;
        else
            start = new Vector3( 1, 1, -( direction.x + direction.y ) / direction.z ).normalized;

        var points = new Vector3[numPoints];
        for( int i = 0; i < numPoints; ++i )
            points[ i ] = radius * ( Quaternion.AngleAxis( 360f * i / ( numPoints - 1 ), direction ) * start );
        return points;
    }

}
