﻿using System;
using UnityEditor;
using UnityEngine;

[CustomEditor( typeof( TubeMeshDisplay ) )]
public class TubeMeshDisplayInspector : Editor
{
    public override void OnInspectorGUI()
    {
        TubeMeshDisplay generator = target as TubeMeshDisplay;
        if( DrawDefaultInspector() )
        {
            generator.UpdateMesh();
        }
        if( GUILayout.Button( "Update" ) )
        {
            generator.UpdateMesh();
        }
    }

    protected virtual void OnSceneGUI()
    {
        TubeMeshDisplay generator = target as TubeMeshDisplay;
        DrawCirclePositionCap( generator, () => generator.BeginDirection, ( v ) => generator.BeginDirection = v );
        DrawCirclePositionCap( generator, () => generator.EndDirection + generator.BeginDirection * generator.Length, ( v ) => generator.EndDirection = v - generator.BeginDirection * generator.Length );
    }

    private static void DrawCirclePositionCap( TubeMeshDisplay generator, Func<Vector3> getter, Action<Vector3> setter )
    {

        float size = HandleUtility.GetHandleSize(getter()) * 0.5f;
        Vector3 snap = Vector3.one * 0.5f;

        EditorGUI.BeginChangeCheck();
        Vector3 newTargetPosition = Handles.FreeMoveHandle(getter(), Quaternion.identity, size, snap, Handles.CircleHandleCap );
        if( EditorGUI.EndChangeCheck() )
        {
            Undo.RecordObject( generator, "Change Look At Target Position" );
            setter( newTargetPosition );
            generator.UpdateMesh();
        }
    }
}
