﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuaternionTest : MonoBehaviour {
    public Transform _handle;

    private void OnDrawGizmos()
    {
        var dir = (_handle.position - transform.position).normalized;
        var q = Quaternion.FromToRotation( transform.right, dir );
        var f = q * transform.forward;
        var u = q * transform.up;
        Gizmos.color = Color.blue;
        Gizmos.DrawLine( transform.position, transform.position + f );
        Gizmos.color = Color.green;
        Gizmos.DrawLine( transform.position, transform.position + u );
        Gizmos.color = Color.red;
        Gizmos.DrawLine( transform.position, transform.position + dir );
    }
}
